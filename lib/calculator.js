window.calculator = window.calculator || {};

(function() {
  var calculate = function() {
    var x = document.getElementById('x').value;
    var y = document.getElementById('y').value;
    document.getElementById('result').innerHTML = parseInt(x) + parseInt(y);
  };

  window.calculator.init = function() {
    document.getElementById('add').addEventListener('click', calculate);
  };
})();